AM_CPPFLAGS = \
	-DGNOMELOCALEDIR=\""$(datadir)/locale"\"        \
	-DG_LOG_DOMAIN=\"netRhythmbox\"		 	\
	-DDATADIR=\""$(datadir)"\"			\
	-DSHARE_DIR=\"$(pkgdatadir)\"			\
	-DPIXMAP_DIR=\""$(datadir)/pixmaps"\"		\
	$(WARN_CFLAGS)					\
	$(RHYTHMBOX_CFLAGS)
