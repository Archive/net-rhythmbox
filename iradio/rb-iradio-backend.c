/* 
 *  Copyright (C) 2002,2003 Colin Walters <walters@gnu.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id: rb-iradio-backend.c,v 1.1 2002/12/10 22:30:52 cwalters Exp $
 */

#include <config.h>
#include <monkey-media.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-init.h>
#include <libxml/tree.h>
#include <gtk/gtkmain.h>
#include <unistd.h>
#include <string.h>
#include <glib.h>
#include <stdio.h>

#include "rb-iradio-backend.h"
#include "rb-iradio-yp-iterator.h"
#include "rb-iradio-yp-xmlfile.h"
#include "rb-debug.h"
#include "rb-dialog.h"
#include "rb-echo-area.h"
#include "rb-file-helpers.h"
#include "rb-stock-icons.h"
#include "rb-node-station.h"
#include "rb-glist-wrapper.h"
#include "rb-library.h"

static void rb_iradio_backend_class_init (RBIRadioBackendClass *klass);
static void rb_iradio_backend_init (RBIRadioBackend *view);
static void rb_iradio_backend_finalize (GObject *object);
static void rb_iradio_backend_set_property (GObject *object,
					    guint prop_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void rb_iradio_backend_get_property (GObject *object,
					    guint prop_id,
					    GValue *value,
					    GParamSpec *pspec);

static void rb_iradio_backend_save (RBIRadioBackend *backend);
static void genre_added_cb (RBNode *node, RBNode *child, RBIRadioBackend *backend);
static void genre_removed_cb (RBNode *node, RBNode *child, RBIRadioBackend *backend);
static void station_added_cb (RBNode *node, RBNode *child, RBIRadioBackend *backend);
static void station_removed_cb (RBNode *node, RBNode *child, RBIRadioBackend *backend);
static RBNode * rb_iradio_backend_lookup_station_by_location (RBIRadioBackend *backend,
							      const char *uri);

#define RB_IRADIO_BACKEND_XML_VERSION "1.0"

struct RBIRadioBackendPrivate
{
	RBNode *all_genres;
	RBNode *all_stations;
	
	GHashTable *genre_hash;
	GStaticRWLock *genre_hash_lock;
	GHashTable *station_hash;
	GStaticRWLock *station_hash_lock;

	RBEchoArea *echoarea;

	char *xml_file;
};

enum
{
	PROP_0,
	PROP_ECHO_AREA,
};

enum
{
	CHANGED,
	LAST_SIGNAL,
};

static GObjectClass *parent_class = NULL;

static guint rb_iradio_backend_signals[LAST_SIGNAL] = { 0 };

GType
rb_iradio_backend_get_type (void)
{
	static GType rb_iradio_backend_type = 0;

	if (rb_iradio_backend_type == 0)
	{
		static const GTypeInfo our_info =
		{
			sizeof (RBIRadioBackendClass),
			NULL,
			NULL,
			(GClassInitFunc) rb_iradio_backend_class_init,
			NULL,
			NULL,
			sizeof (RBIRadioBackend),
			0,
			(GInstanceInitFunc) rb_iradio_backend_init
		};

		rb_iradio_backend_type = g_type_register_static (G_TYPE_OBJECT,
								 "RBIRadioBackend",
								 &our_info, 0);
		
	}

	return rb_iradio_backend_type;
}

static void
rb_iradio_backend_class_init (RBIRadioBackendClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	
	parent_class = g_type_class_peek_parent (klass);
	
	object_class->set_property = rb_iradio_backend_set_property;
	object_class->get_property = rb_iradio_backend_get_property;

	g_object_class_install_property (object_class,
					 PROP_ECHO_AREA,
					 g_param_spec_object ("echoarea",
							      "Echo area",
							      "Echo area",
							      RB_TYPE_ECHO_AREA,
							      G_PARAM_READWRITE));

	rb_iradio_backend_signals[CHANGED] =
		g_signal_new ("changed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (RBIRadioBackendClass, changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE,
			      0);

	object_class->finalize = rb_iradio_backend_finalize;

}

static void
rb_iradio_backend_init (RBIRadioBackend *backend)
{
	char *libname = g_strdup_printf ("iradio-%s.xml", RB_IRADIO_BACKEND_XML_VERSION);
	GValue value = { 0, };

	/* ensure these types have been registered: */
	rb_node_get_type ();
	rb_glist_wrapper_get_type ();
	rb_node_station_get_type ();

	backend->priv = g_new0 (RBIRadioBackendPrivate, 1);
	backend->priv->xml_file = g_build_filename (rb_dot_dir (),
						    libname,
						    NULL);

	g_free (libname);

	backend->priv->genre_hash = g_hash_table_new (g_str_hash,
						      g_str_equal);
	backend->priv->station_hash = g_hash_table_new (g_str_hash,
							g_str_equal);

	backend->priv->genre_hash_lock = g_new0 (GStaticRWLock, 1);
	g_static_rw_lock_init (backend->priv->genre_hash_lock);
	backend->priv->station_hash_lock = g_new0 (GStaticRWLock, 1);
	g_static_rw_lock_init (backend->priv->station_hash_lock);

	backend->priv->all_genres  = rb_node_new ();
	backend->priv->all_stations = rb_node_new ();

	g_signal_connect_object (G_OBJECT (backend->priv->all_genres),
				 "child_added",
				 G_CALLBACK (genre_added_cb),
				 G_OBJECT (backend),
				 0);
	g_signal_connect_object (G_OBJECT (backend->priv->all_genres),
				 "child_removed",
				 G_CALLBACK (genre_removed_cb),
				 G_OBJECT (backend),
				 0);
	g_signal_connect_object (G_OBJECT (backend->priv->all_stations),
				 "child_added",
				 G_CALLBACK (station_added_cb),
				 G_OBJECT (backend),
				 0);
	g_signal_connect_object (G_OBJECT (backend->priv->all_stations),
				 "child_removed",
				 G_CALLBACK (station_removed_cb),
				 G_OBJECT (backend),
				 0);

	rb_node_ref (backend->priv->all_genres);
	rb_node_ref (backend->priv->all_stations);

	g_value_init (&value, G_TYPE_STRING);
	g_value_set_string (&value, _("All"));
	rb_node_set_property (backend->priv->all_genres,
			      RB_NODE_PROP_NAME,
			      &value);
	rb_node_set_property (backend->priv->all_stations,
			      RB_NODE_PROP_NAME,
			      &value);
	rb_node_set_property (backend->priv->all_genres,
			      RB_NODE_PROP_GENRE,
			      &value);
	rb_node_set_property (backend->priv->all_stations,
			      RB_NODE_PROP_GENRE,
			      &value);
	g_value_unset (&value);

	g_value_init (&value, G_TYPE_BOOLEAN);
	g_value_set_boolean (&value, TRUE);
	rb_node_set_property (backend->priv->all_genres,
			      RB_ALL_NODE_PROP_PRIORITY,
			      &value);
	rb_node_set_property (backend->priv->all_stations,
			      RB_ALL_NODE_PROP_PRIORITY,
			      &value);
	g_value_unset (&value);

	rb_node_add_child (backend->priv->all_genres,
			   backend->priv->all_stations);
}

static void
rb_iradio_backend_set_property (GObject *object,
				guint prop_id,
				const GValue *value,
				GParamSpec *pspec)
{
	RBIRadioBackend *backend = RB_IRADIO_BACKEND (object);

	switch (prop_id)
	{
	case PROP_ECHO_AREA:
		backend->priv->echoarea = g_value_get_object (value);
		g_object_ref (backend->priv->echoarea);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
rb_iradio_backend_get_property (GObject *object,
				guint prop_id,
				GValue *value,
				GParamSpec *pspec)
{
	RBIRadioBackend *backend = RB_IRADIO_BACKEND (object);

	switch (prop_id)
	{
	case PROP_ECHO_AREA:
		g_value_set_object (value, backend->priv->echoarea);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
rb_iradio_backend_finalize (GObject *object)
{
	int i;
	RBIRadioBackend *backend;
	GPtrArray *children;

	g_return_if_fail (object != NULL);
	g_return_if_fail (RB_IS_IRADIO_BACKEND (object));

	backend = RB_IRADIO_BACKEND (object);

	g_return_if_fail (backend->priv != NULL);

	rb_iradio_backend_save (backend);

	children = rb_node_get_children (backend->priv->all_stations);
	rb_node_thaw (backend->priv->all_stations);
	for (i = children->len - 1; i >= 0; i--) {
		rb_node_unref (g_ptr_array_index (children, i));
	}
	
	rb_node_unref (backend->priv->all_stations);
	rb_node_unref (backend->priv->all_genres);
	g_free (backend->priv->xml_file);
	g_free (backend->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

RBNode *
rb_iradio_backend_get_genre_by_name (RBIRadioBackend *backend,
				     const char *genre)
{
	RBNode *ret;
	
	g_static_rw_lock_reader_lock (backend->priv->genre_hash_lock);
	
	ret = g_hash_table_lookup (backend->priv->genre_hash,
				   genre);
	
	g_static_rw_lock_reader_unlock (backend->priv->genre_hash_lock);

	return ret;
}

RBNode *
rb_iradio_backend_get_all_genres (RBIRadioBackend *backend)
{
	return backend->priv->all_genres;
}

RBNode *
rb_iradio_backend_get_all_stations (RBIRadioBackend *backend)
{
	return backend->priv->all_stations;
}

int
rb_iradio_backend_get_genre_count (RBIRadioBackend *backend)
{
	/* Subtract "All" */
	return g_hash_table_size (backend->priv->genre_hash) - 1;
}

int 
rb_iradio_backend_get_station_count (RBIRadioBackend *backend)
{
	GPtrArray *children = rb_node_get_children (backend->priv->all_stations);
	int ret;
	ret = children->len;
	rb_node_thaw (backend->priv->all_stations);
	return ret;
}

static void
genre_added_cb (RBNode *node,
		RBNode *child,
		RBIRadioBackend *backend)
{
	g_static_rw_lock_writer_lock (backend->priv->genre_hash_lock);

	g_hash_table_insert (backend->priv->genre_hash,
			     (char *) rb_node_get_property_string (child, RB_NODE_PROP_NAME),
			     child);
	
	g_static_rw_lock_writer_unlock (backend->priv->genre_hash_lock);
}

static void
genre_removed_cb (RBNode *node,
		  RBNode *child,
		  RBIRadioBackend *backend)
{
	g_static_rw_lock_writer_lock (backend->priv->genre_hash_lock);
	
	g_hash_table_remove (backend->priv->genre_hash,
			     rb_node_get_property_string (child, RB_NODE_PROP_NAME));
	
	g_static_rw_lock_writer_unlock (backend->priv->genre_hash_lock);
}

static void
station_added_cb (RBNode *node,
		  RBNode *child,
		  RBIRadioBackend *backend)
{
	const char *location;
	RBGListWrapper *alt_locations;
	GList *l;
	g_static_rw_lock_writer_lock (backend->priv->station_hash_lock);

	location = rb_node_get_property_string (child, RB_NODE_PROP_LOCATION);
	alt_locations = RB_GLIST_WRAPPER (rb_node_get_property_object (child, RB_NODE_PROP_ALT_LOCATIONS));

	g_hash_table_insert (backend->priv->station_hash,
			     (char*) location,
			     child);
	if (alt_locations)
		for (l = rb_glist_wrapper_get_list (alt_locations); l; l = l->next)
			g_hash_table_insert (backend->priv->station_hash,
					     (char*) l->data,
					     child);
		
	
	g_static_rw_lock_writer_unlock (backend->priv->station_hash_lock);
}

static void
station_removed_cb (RBNode *node,
		    RBNode *child,
		    RBIRadioBackend *backend)
{
	const char *location;
	RBGListWrapper *alt_locations;
	GList *l;
	g_static_rw_lock_writer_lock (backend->priv->station_hash_lock);
	
	location = rb_node_get_property_string (child, RB_NODE_PROP_LOCATION);
	alt_locations = RB_GLIST_WRAPPER (rb_node_get_property_object (child, RB_NODE_PROP_ALT_LOCATIONS));

	g_hash_table_remove (backend->priv->station_hash,
			     location);
	if (alt_locations)
		for (l = rb_glist_wrapper_get_list (alt_locations); l; l = l->next)
			g_hash_table_remove (backend->priv->station_hash,
					     (char*) l->data);
	
	g_static_rw_lock_writer_unlock (backend->priv->station_hash_lock);
}

static void
load_initial (RBIRadioBackend *backend)
{
	guint taskid;
	const char *initial_file = rb_file ("iradio-initial.xml");
	RBIRadioYPIterator *it;
	RBIRadioStation *station;

	if (!initial_file)
	{
		rb_error_dialog (_("Unable to find file \"iradio-initial.xml\""));
		return;
	}
	
	it = RB_IRADIO_YP_ITERATOR (g_object_new (RB_TYPE_IRADIO_YP_XMLFILE,
						  "filename", initial_file,
						  NULL));

	taskid = rb_echo_area_begin_task (backend->priv->echoarea,
					  _("Loading Internet Radio stations..."));

	rb_debug ("iradio-backend: loading initial stations");
	while ((station = rb_iradio_yp_iterator_get_next_station (it)) != NULL) {
		const char *genre = NULL;
		const char *name = NULL;
		GList *locations;
		RBNodeStation *nodestation;

		g_assert (RB_IS_IRADIO_STATION (station));

		g_object_get (G_OBJECT(station), "genre", &genre, NULL);
		g_assert (genre != NULL);
		g_object_get (G_OBJECT(station), "name", &name, NULL);
		g_assert (name != NULL);
		g_object_get (G_OBJECT(station), "locations", &locations, NULL);

		g_assert (G_IS_OBJECT (backend));
		g_assert (RB_IS_IRADIO_BACKEND (backend));
		nodestation = rb_node_station_new (locations, name, genre, "initial", backend);
	}
	rb_echo_area_end_task (backend->priv->echoarea, taskid);
	rb_debug ("iradio-backend: done loading initial stations");
	g_free ((char *) initial_file);
	g_object_unref (G_OBJECT (it));
	g_signal_emit (G_OBJECT (backend), rb_iradio_backend_signals[CHANGED], 0);
}
	
void rb_iradio_backend_load (RBIRadioBackend *backend)
{
	xmlDocPtr doc;
	xmlNodePtr root, child;
	char *tmp;
	guint taskid;

	rb_debug ("iradio-backend: loading");

	taskid = rb_echo_area_begin_task (backend->priv->echoarea,
					  _("Loading Internet Radio stations..."));

	if (g_file_test (backend->priv->xml_file, G_FILE_TEST_EXISTS) == FALSE)
		goto loadinitial;
	
	doc = xmlParseFile (backend->priv->xml_file);

	if (doc == NULL)
	{
		rb_error_dialog (_("Failed to parse %s\n"), backend->priv->xml_file);
		unlink (backend->priv->xml_file);
		goto loadinitial;
	}


	root = xmlDocGetRootElement (doc);

	tmp = xmlGetProp (root, "version");
	if (tmp == NULL || strcmp (tmp, RB_IRADIO_BACKEND_XML_VERSION) != 0)
	{
		fprintf (stderr, "Invalid version in %s\n", backend->priv->xml_file);
		g_free (tmp);
		unlink (backend->priv->xml_file);
		xmlFreeDoc (doc);
		goto loadinitial;
	}
	g_free (tmp);

	for (child = root->children; child != NULL; child = child->next)
	{
		/* This automagically sets up the tree structure */
		RBNode *node = rb_node_new_from_xml (child);
		rb_debug ("iradio-backend: loaded node %p", node);
		if (node != NULL)
		{
			if (RB_IS_NODE_STATION (node))
			{
/* 			rb_node_station_sync (node, backend); */
			}
		}
	}
	rb_echo_area_end_task (backend->priv->echoarea, taskid);
	xmlFreeDoc (doc);
	g_signal_emit (G_OBJECT (backend), rb_iradio_backend_signals[CHANGED], 0);
	return;
 loadinitial:
	rb_echo_area_end_task (backend->priv->echoarea, taskid);
	load_initial (backend);
}

static void
rb_iradio_backend_save (RBIRadioBackend *backend)
{
	xmlDocPtr doc;
	xmlNodePtr root;
	GPtrArray *children;
	int i;

	rb_debug ("iradio-backend: saving");

	/* save nodes to xml */
	xmlIndentTreeOutput = TRUE;
	doc = xmlNewDoc ("1.0");

	root = xmlNewDocNode (doc, NULL, "rhythmbox_iradio", NULL);
	xmlSetProp (root, "version", RB_IRADIO_BACKEND_XML_VERSION);
	xmlDocSetRootElement (doc, root);

	children = rb_node_get_children (backend->priv->all_genres);
	for (i = 0; i < children->len; i++)
	{
		RBNode *kid;

		kid = g_ptr_array_index (children, i);
		
		if (kid != backend->priv->all_stations)
		{
			rb_node_save_to_xml (kid, root);
		}
	}
	rb_node_thaw (backend->priv->all_genres);

	children = rb_node_get_children (backend->priv->all_stations);
	for (i = 0; i < children->len; i++)
	{
		RBNode *kid;

		kid = g_ptr_array_index (children, i);
		
		rb_node_save_to_xml (kid, root);
	}
	rb_node_thaw (backend->priv->all_stations);

	xmlSaveFormatFile (backend->priv->xml_file, doc, 1);
	rb_debug ("iradio-backend: done saving");
}

void
rb_iradio_backend_remove_node (RBIRadioBackend *backend,
			       RBNode *node)
{
	rb_node_unref (RB_NODE (node));
}

/* Returns a locked RBNode */
static RBNode *
rb_iradio_backend_lookup_station_by_title (RBIRadioBackend *backend,
					  const char *title)
{
	int i;
	RBNode *retval = NULL;
	GPtrArray *children = rb_node_get_children (backend->priv->all_stations);
	for (i = 0; retval == NULL && i < children->len; i++)
	{
		RBNode *kid = g_ptr_array_index (children, i);
		if (!strcmp (title, rb_node_get_property_string (kid, RB_NODE_PROP_NAME)))
		{
			retval = kid;
			break;
		}
	}

	rb_node_thaw (backend->priv->all_stations);
	return retval;
}

static RBNode *
rb_iradio_backend_lookup_station_by_location (RBIRadioBackend *backend,
					      const char *uri)
{
	RBNode *ret;
	
	g_static_rw_lock_reader_lock (backend->priv->station_hash_lock);
	
	ret = g_hash_table_lookup (backend->priv->station_hash,
				   uri);
	
	g_static_rw_lock_reader_unlock (backend->priv->station_hash_lock);

	return ret;
}

void
rb_iradio_backend_add_station_from_uri (RBIRadioBackend *backend,
					const char *uri)
{
	RBNode *station = rb_iradio_backend_lookup_station_by_location (backend, uri);
	if (station == NULL)
	{
		GList *locations = g_list_append (NULL, g_strdup (uri));
		rb_iradio_backend_add_station_full (backend, locations,
						    _("(Unknown)"), _("(Unknown)"));
	}
}


void
rb_iradio_backend_add_station_full (RBIRadioBackend *backend,
				    GList *locations,
				    const char *name,
				    const char *genre)
{
	RBNode *node = rb_iradio_backend_lookup_station_by_title (backend, name);

	if (node == NULL)
	{
		rb_debug ("iradio-backend: adding station; name: %s genre: %s",
			  name, genre);
		node = RB_NODE (rb_node_station_new (locations,
						     name ? name : _("(Unknown)"),
						     genre ? genre : _("(Unknown)"),
						     "user", backend));
	}
	else
		rb_debug ("iradio-backend: station %s already exists", name);
}

GList *
rb_iradio_backend_get_genre_names (RBIRadioBackend *backend)
{
	GList *genrenames = NULL;
	RBNode *genres = rb_iradio_backend_get_all_genres (backend);
	GPtrArray *children = rb_node_get_children (backend->priv->all_genres);
	int i;
	for (i = 0; i < children->len; i++)
	{
		RBNode *kid;
		const char *name;
		
		kid = g_ptr_array_index (children, i);
		
		name = rb_node_get_property_string (kid, RB_NODE_PROP_NAME);
		if (strcmp (name, "All"))
		{
			/* FIXME memory leak here? */
			genrenames = g_list_append (genrenames,
						    g_strdup (name));
		}
	}
	rb_node_thaw (genres);
	return genrenames;
}
