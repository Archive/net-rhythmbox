/*  monkey-sound
 *  Copyright (C) 2002 Jorn Baayen <jorn@nl.linux.org>
 *                     Marco Pesenti Gritti <marco@it.gnome.org>
 *                     Bastien Nocera <hadess@hadess.net>
 *                     Seth Nickell <snickell@stanford.edu>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id: monkey-media-audio-cd-private.h,v 1.2 2002/09/24 18:23:28 jbaayen Exp $
 */

#ifndef __MONKEY_MEDIA_AUDIO_CD_PRIVATE_H
#define __MONKEY_MEDIA_AUDIO_CD_PRIVATE_H

#include "monkey-media-audio-cd.h"

G_BEGIN_DECLS

struct MonkeyMediaAudioCDPrivate 
{
	int open_count;

        int poll_func_id;

	GError *error;

	int fd;

        gboolean valid_info;

	gboolean cd_available;
	
        int n_audio_tracks;
        int *track_lengths;
        int *track_offsets;

	char *cd_id;

	GMutex *lock;
};

enum
{
	PROP_0,
	PROP_ERROR
};

enum
{
	CD_CHANGED,
	LAST_SIGNAL
};

long     monkey_media_audio_cd_get_track_duration (MonkeyMediaAudioCD *cd,
					           int track,
					           GError **error);
int      monkey_media_audio_cd_get_track_offset   (MonkeyMediaAudioCD *cd,
					           int track,
					           GError **error);
gboolean monkey_media_audio_cd_have_track         (MonkeyMediaAudioCD *cd,
						   int track,
						   GError **error);
int      monkey_media_audio_cd_get_n_tracks       (MonkeyMediaAudioCD *cd,
						   GError **error);

void     monkey_media_audio_cd_unref_if_around    (void);

void     monkey_media_audio_cd_close (MonkeyMediaAudioCD *cd, 
				      gboolean force_close);

char *   monkey_media_audio_cd_calculate_musicbrainz_id (unsigned char track0,
							 unsigned char track1,
							 long *frame_offests);
/**
 * Subsystem-specific prototypes
 */
gboolean monkey_media_audio_cd_is_cdrom_device_impl (MonkeyMediaAudioCD *cdrom);

gboolean monkey_media_audio_cd_device_available_impl ();

gboolean monkey_media_audio_cd_open_impl (MonkeyMediaAudioCD *cd,
					  GError **error);

void     monkey_media_audio_cd_close_impl (MonkeyMediaAudioCD *cd);

gboolean monkey_media_audio_cd_open_tray_impl (MonkeyMediaAudioCD *cd,
					       GError **error);

gboolean monkey_media_audio_cd_close_tray_impl (MonkeyMediaAudioCD *cd,
						GError **error);

gboolean monkey_media_audio_cd_available_impl (MonkeyMediaAudioCD *cd,
					       GError **error);

gboolean monkey_media_audio_cd_ensure_sync_impl (MonkeyMediaAudioCD *cd,
						 GError **error);

gboolean monkey_media_audio_cd_poll_event_impl (MonkeyMediaAudioCD *cd);


G_END_DECLS

#endif /* __MONKEY_MEDIA_AUDIO_CD_PRIVATE_H */
