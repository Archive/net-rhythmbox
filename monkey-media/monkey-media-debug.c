/*
 *  Copyright (C) 2002 Jorn Baayen
 *  Copyright (C) 2003 Colin Walters <walters@verbum.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  NOTES: log domain hack stolen from nautilus
 *
 *  $Id: rb-debug.c,v 1.2 2002/08/18 20:23:36 jbaayen Exp $
 */

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <time.h>

#include "monkey-media-debug.h"

static gboolean debugging = FALSE;

/* Our own funky debugging function, should only be used when something
 * is not going wrong, if something *is* wrong use g_warning.
 */
void
monkey_media_debug_real (const char *func,
		  	 const char *file,
			 const int line,
			 const char *format, ...)
{
	va_list args;
	char buffer[1025];
	char *str_time;
	time_t the_time;

	if (debugging == FALSE) return;

	va_start (args, format);

	vsnprintf (buffer, 1024, format, args);
	
	va_end (args);

	time (&the_time);
	str_time = g_new0 (char, 255);
	strftime (str_time, 254, "%H:%M:%S", localtime (&the_time));

	g_printerr ("[%s] %s:%d (%s): %s\n", func, file, line, str_time, buffer);
	
	g_free (str_time);
}

void
monkey_media_debug_init (gboolean debug)
{
	debugging = debug;

	monkey_media_debug ("Debugging enabled");
}
