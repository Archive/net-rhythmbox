/*  monkey-media
 *  Copyright (C) 2001 Iain Holmes <iain@ximian.com>
 *                2002 Kenneth Christiansen <kenneth@gnu.org>
 *                     Olivier Martin <omartin@ifrance.com>
 *                     Jorn Baayen <jorn@nl.linux.org>
 *                2003 Colin Walters <walters@verbum.org>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include <config.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <linux/cdrom.h>

#include "monkey-media.h"
#include "monkey-media-private.h"
#include "monkey-media-audio-cd.h"
#include "monkey-media-audio-cd-private.h"

gboolean
monkey_media_audio_cd_is_cdrom_device_impl (MonkeyMediaAudioCD *cdrom)
{
	int fd;

	fd = open (monkey_media_get_cd_drive (), O_RDONLY | O_NONBLOCK);
	if (fd < 0) 
	{
		return FALSE;
	}

	/* Fire a harmless ioctl at the device. */
	if (ioctl (fd, CDROM_GET_CAPABILITY, 0) < 0)
	{
		/* Failed, it's not a CDROM drive */
		close (fd);
		
		return FALSE;
	}
	
	close (fd);

	return TRUE;
}

gboolean
monkey_media_audio_cd_device_available_impl ()
{
	int fd;
	const char *drive = monkey_media_get_cd_drive ();

	fd = open (drive, O_RDONLY | O_NONBLOCK);
	if (fd < 0) 
	{
		monkey_media_debug ("Failed to open %s: %s", drive,
				    strerror (errno));
		return FALSE;
	}

	/* Fire a harmless ioctl at the device. */
	if (ioctl (fd, CDROM_GET_CAPABILITY, 0) < 0)
	{
		/* Failed, it's not a CDROM drive */
		close (fd);
		
		monkey_media_debug ("CDROM_GET_CAPABILITY failed for %s", drive);
		return FALSE;
	}
	
	close (fd);

	return TRUE;
}

gboolean
monkey_media_audio_cd_open_impl (MonkeyMediaAudioCD *cd,
				 GError **error)
{
	if (cd->priv->open_count++ == 0)
	{
		cd->priv->fd = open (monkey_media_get_cd_drive (), O_RDONLY | O_NONBLOCK);
		
		if (cd->priv->fd < 0) 
		{
			if (errno == EACCES && error != NULL)
			{
				*error = g_error_new (MONKEY_MEDIA_AUDIO_CD_ERROR,
						      MONKEY_MEDIA_AUDIO_CD_ERROR_NOT_OPENED,
						      _("You do not seem to have permission to access %s."),
						      monkey_media_get_cd_drive ());
			} 
			else if (error != NULL) 
			{
				*error = g_error_new (MONKEY_MEDIA_AUDIO_CD_ERROR,
						      MONKEY_MEDIA_AUDIO_CD_ERROR_NOT_OPENED,
						      _("%s does not appear to point to a valid CD device. This may be because:\n"
							"a) CD support is not compiled into Linux\n"
							"b) You do not have the correct permissions to access the CD drive\n"
							"c) %s is not the CD drive.\n"),
						      monkey_media_get_cd_drive (),
						      monkey_media_get_cd_drive ());
			}
				
			cd->priv->open_count = 0;
			return FALSE;
		}
	
	}
	return (cd->priv->fd >= 0);
}

void
monkey_media_audio_cd_close_impl (MonkeyMediaAudioCD *cd)
{
	if (cd->priv->open_count == 0) 
	{
		if (cd->priv->fd >= 0)
			close (cd->priv->fd);
		cd->priv->fd = -1;
	}
}

gboolean
monkey_media_audio_cd_open_tray_impl (MonkeyMediaAudioCD *cd,
				      GError **error)
{
	int cd_status;

	cd_status = ioctl (cd->priv->fd, CDROM_DRIVE_STATUS, CDSL_CURRENT);

	if (cd_status != -1 && cd_status != CDS_TRAY_OPEN
	    && ioctl (cd->priv->fd, CDROMEJECT, 0) < 0)
	{
		if (error != NULL)
		{
			*error = g_error_new (MONKEY_MEDIA_AUDIO_CD_ERROR,
					      MONKEY_MEDIA_AUDIO_CD_ERROR_SYSTEM_ERROR,
					      "(monkey_media_audio_cd_open_tray): ioctl failed: %s",
					      g_strerror (errno));
			return TRUE;
		}
	}
	return FALSE;
}

gboolean
monkey_media_audio_cd_close_tray_impl (MonkeyMediaAudioCD *cd,
				       GError **error)
{
	if (ioctl (cd->priv->fd, CDROMCLOSETRAY) < 0) 
	{
		if (error != NULL) 
		{
			*error = g_error_new (MONKEY_MEDIA_AUDIO_CD_ERROR,
					      MONKEY_MEDIA_AUDIO_CD_ERROR_SYSTEM_ERROR,
					      "(monkey_media_audio_cd_close_tray): ioctl failed %s",
					      g_strerror (errno));
			return TRUE;
		}
	}
	return FALSE;	
}

gboolean
monkey_media_audio_cd_available_impl (MonkeyMediaAudioCD *cd,
				      GError **error)
{
	int cd_status = ioctl (cd->priv->fd, CDROM_DRIVE_STATUS, CDSL_CURRENT);

	return (cd_status == CDS_DISC_OK);
}

gboolean
monkey_media_audio_cd_ensure_sync_impl (MonkeyMediaAudioCD *cd,
					GError **error)
{
	int i, j, *track_frames;
	struct cdrom_tochdr tochdr;
	unsigned char track0, track1;
	struct cdrom_tocentry tocentry;
	long *frame_offsets;
 
	monkey_media_debug ("Trying CDROMREADTOCHDR");
	if (ioctl (cd->priv->fd, CDROMREADTOCHDR, &tochdr) < 0) 
	{
		if (error != NULL)
		{
			*error = g_error_new (MONKEY_MEDIA_AUDIO_CD_ERROR,
					      MONKEY_MEDIA_AUDIO_CD_ERROR_SYSTEM_ERROR,
					      _("Error reading CD header: %s"),
					      g_strerror (errno));
		}
		monkey_media_audio_cd_close (cd, FALSE);
		monkey_media_debug ("CDROMREADTOCHDR failed!");
		return FALSE;
	}
	
	track0 = tochdr.cdth_trk0;
	track1 = tochdr.cdth_trk1;
	cd->priv->n_audio_tracks = track1 - track0 + 1;
	monkey_media_debug ("Got %d tracks", cd->priv->n_audio_tracks);

        if (cd->priv->track_offsets != NULL)
        {
                g_free (cd->priv->track_offsets);
                g_free (cd->priv->track_lengths);
        }

	cd->priv->track_offsets = g_new0 (int, cd->priv->n_audio_tracks + 1);
        cd->priv->track_lengths = g_new0 (int, cd->priv->n_audio_tracks + 1);

	frame_offsets = g_new0 (long, 100);
	track_frames = g_new0 (int, cd->priv->n_audio_tracks + 1);

	for (i = 0, j = track0; i < cd->priv->n_audio_tracks; i++, j++) 
	{
		/* handle time-based stuff */
		tocentry.cdte_track = j;
		tocentry.cdte_format = CDROM_MSF;

		monkey_media_debug ("Trying CDROMREADTOCENTRY: %d, CDROM_MSF", j);
		if (ioctl (cd->priv->fd, CDROMREADTOCENTRY, &tocentry) < 0) 
		{
			g_warning ("IOCtl failed");
			continue;
		}

		cd->priv->track_offsets[i] = (tocentry.cdte_addr.msf.minute * 60) 
                        + tocentry.cdte_addr.msf.second;
		track_frames[i] = tocentry.cdte_addr.msf.frame;

		/* get frame offest */
		tocentry.cdte_track = j;
		tocentry.cdte_format = CDROM_LBA;

		monkey_media_debug ("Trying CDROMREADTOCENTRY: %d, CDROM_LBA", j);
		if (ioctl (cd->priv->fd, CDROMREADTOCENTRY, &tocentry) < 0) 
		{
			g_warning ("IOCtl failed");
			continue;
		}
		
		frame_offsets[i + 1] = tocentry.cdte_addr.lba + 150;
	}

	/* handle time based stuff */
	tocentry.cdte_track = CDROM_LEADOUT;
	tocentry.cdte_format = CDROM_MSF;
	
	monkey_media_debug ("Trying CDROMREADTOCENTRY with CDROM_LEADOUT, CDROM_MSF");
	if (ioctl (cd->priv->fd, CDROMREADTOCENTRY, &tocentry) < 0) 
		goto leadout_error;
	
	cd->priv->track_offsets[cd->priv->n_audio_tracks] = (tocentry.cdte_addr.msf.minute * 60) 
                + tocentry.cdte_addr.msf.second;
	track_frames[cd->priv->n_audio_tracks] = tocentry.cdte_addr.msf.frame;

	/* get frame offset */
	tocentry.cdte_track = CDROM_LEADOUT;
	tocentry.cdte_format = CDROM_LBA;

	monkey_media_debug ("Trying CDROMREADTOCENTRY with CDROM_LEADOUT, CDROM_LBA");
	if (ioctl (cd->priv->fd, CDROMREADTOCENTRY, &tocentry) < 0)
		goto leadout_error;
	
	frame_offsets[0] = tocentry.cdte_addr.lba + 150;

        for (i = 0; i < cd->priv->n_audio_tracks; i++) 
        {
                int f1, f2, df;
                
                /* Convert all addresses to frames */
                f1 = cd->priv->track_offsets[i] * CD_FRAMES + track_frames[i];
                f2 = cd->priv->track_offsets[i + 1] * CD_FRAMES + track_frames[i + 1];
                
                df = f2 - f1;
                cd->priv->track_lengths[i] = df / CD_FRAMES;
        }

	if (cd->priv->cd_id != NULL)
		g_free (cd->priv->cd_id);
	cd->priv->cd_id = monkey_media_audio_cd_calculate_musicbrainz_id (track0,
									  track1,
									  frame_offsets);
	monkey_media_debug ("Calculated MusicBrainz ID \"%s\"",
			    cd->priv->cd_id);

	g_free (track_frames);
	g_free (frame_offsets);

	monkey_media_audio_cd_close (cd, TRUE);

        cd->priv->valid_info = TRUE;

	return TRUE;

leadout_error:
	if (error != NULL)
	{
		*error = g_error_new (MONKEY_MEDIA_AUDIO_CD_ERROR,
				      MONKEY_MEDIA_AUDIO_CD_ERROR_SYSTEM_ERROR,
				      _("Error getting leadout: %s"),
				      g_strerror (errno));
	}

	monkey_media_audio_cd_close (cd, FALSE);

	g_free (track_frames);
	g_free (frame_offsets);

	return FALSE;
}

gboolean
monkey_media_audio_cd_poll_event_impl (MonkeyMediaAudioCD *cd)
{
	int cd_status;
	gboolean emit_signal = FALSE;
	cd_status = ioctl (cd->priv->fd, CDROM_DRIVE_STATUS, CDSL_CURRENT);
	if (cd_status != -1) 
	{
		switch (cd_status) 
		{
		case CDS_NO_INFO:
		case CDS_NO_DISC:
		case CDS_TRAY_OPEN:
		case CDS_DRIVE_NOT_READY:
			if (cd->priv->cd_available == TRUE)
			{
				cd->priv->cd_available = FALSE;
				cd->priv->valid_info = FALSE;

				monkey_media_debug ("CD not available");
				emit_signal = TRUE;
			}
                        break;
		default:
			if (cd->priv->cd_available == FALSE)
			{
				monkey_media_debug ("Got CD available!");
				cd->priv->cd_available = TRUE;
				cd->priv->valid_info = FALSE;

				emit_signal = TRUE;
                        }
			break;
		}
	} 
	else
	{
		/* so we went out of sync.. */
		cd->priv->valid_info = FALSE;
	}
	return emit_signal;
}
