/* 
 *  Copyright (C) 2003 Colin Walters <walters@debian.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id: rb-link.c,v 1.11 2002/12/03 16:50:30 cwalters Exp $
 */

#include <gtk/gtkstatusbar.h>
#include <gtk/gtkprogressbar.h>
#include <config.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-url.h>
#include <string.h>
#include <stdio.h>

#include "rb-echo-area.h"
#include "rb-debug.h"
#include "rb-thread-helpers.h"

static void rb_echo_area_class_init (RBEchoAreaClass *klass);
static void rb_echo_area_init (RBEchoArea *echoarea);
static void rb_echo_area_finalize (GObject *object);

struct RBEchoAreaTaskContext
{
	guint id;
	GtkStatusbar *statusbar;
};

struct RBEchoAreaMsgContext
{
	guint id;
	RBEchoArea *echoarea;
};

static gboolean pop_message (gpointer data);
static gboolean pulse_status (gpointer data);

struct RBEchoAreaPrivate
{
	GtkStatusbar *statusbar;
	GtkProgressBar *progressbar;

	gboolean perm_msg_active;
	guint perm_id;
	gboolean priority_msg_active;
	GList *deferred_msgs;

	guint active_tasks;
	guint active_tasks_ids;
	GHashTable *active_task_statusbars;
};

static GObjectClass *parent_class = NULL;

GType
rb_echo_area_get_type (void)
{
	static GType rb_echo_area_type = 0;

	if (rb_echo_area_type == 0)
	{
		static const GTypeInfo our_info =
		{
			sizeof (RBEchoAreaClass),
			NULL,
			NULL,
			(GClassInitFunc) rb_echo_area_class_init,
			NULL,
			NULL,
			sizeof (RBEchoArea),
			0,
			(GInstanceInitFunc) rb_echo_area_init
		};

		rb_echo_area_type = g_type_register_static (GTK_TYPE_HBOX,
							    "RBEchoArea",
							    &our_info, 0);
	}

	return rb_echo_area_type;
}

static void
rb_echo_area_class_init (RBEchoAreaClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = rb_echo_area_finalize;
}

static void
rb_echo_area_init (RBEchoArea *echoarea)
{
	echoarea->priv = g_new0 (RBEchoAreaPrivate, 1);
	echoarea->priv->active_tasks = 0;
	echoarea->priv->active_task_statusbars = g_hash_table_new (g_direct_hash, g_direct_equal);
	echoarea->priv->statusbar = GTK_STATUSBAR (gtk_statusbar_new ());
	gtk_statusbar_set_has_resize_grip (echoarea->priv->statusbar, FALSE);
	echoarea->priv->progressbar = GTK_PROGRESS_BAR (gtk_progress_bar_new ());

	g_object_ref (G_OBJECT (echoarea->priv->statusbar));
	g_object_ref (G_OBJECT (echoarea->priv->progressbar));

	gtk_box_pack_start_defaults (GTK_BOX (echoarea), GTK_WIDGET (echoarea->priv->statusbar));
	gtk_box_pack_start (GTK_BOX (echoarea), GTK_WIDGET (echoarea->priv->progressbar),
			    FALSE, FALSE, 0);

	gtk_progress_bar_set_fraction (echoarea->priv->progressbar, 1.0);
}

static void
rb_echo_area_finalize (GObject *object)
{
	RBEchoArea *echoarea;

	g_return_if_fail (object != NULL);
	g_return_if_fail (RB_IS_ECHO_AREA (object));

	echoarea = RB_ECHO_AREA (object);

	g_return_if_fail (echoarea->priv != NULL);

	g_hash_table_destroy (echoarea->priv->active_task_statusbars);

	g_object_unref (G_OBJECT (echoarea->priv->statusbar));
	g_object_unref (G_OBJECT (echoarea->priv->progressbar));
	g_free (echoarea->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

RBEchoArea *
rb_echo_area_new (void)
{
	RBEchoArea *echoarea;

	echoarea = RB_ECHO_AREA (g_object_new (RB_TYPE_ECHO_AREA, NULL));

	g_return_val_if_fail (echoarea->priv != NULL, NULL);

	return echoarea;
}


static gboolean
pop_message (gpointer data)
{
	struct RBEchoAreaMsgContext *ctx = data;
	RBEchoArea *echoarea = RB_ECHO_AREA (ctx->echoarea);

	gdk_threads_enter ();

	rb_debug ("echo area: popping message id %d", ctx->id);
	gtk_statusbar_remove (echoarea->priv->statusbar, 0, ctx->id);

	g_object_unref (G_OBJECT (echoarea));

	gdk_threads_leave ();

	g_free (ctx);
	return FALSE;
}

void
rb_echo_area_msg (RBEchoArea *echoarea,
		  const char *text)
{
	rb_echo_area_msg_full (echoarea, text, 2000);
}

void
rb_echo_area_msg_full (RBEchoArea *echoarea,
		       const char *text,
		       guint timeout)
{
	g_return_if_fail (RB_IS_ECHO_AREA (echoarea));
	g_return_if_fail (text != NULL);

	rb_thread_helpers_lock_gdk ();
	
	if (timeout != 0) {
		struct RBEchoAreaMsgContext *ctx = g_new0 (struct RBEchoAreaMsgContext, 1);
		ctx->id = gtk_statusbar_push (echoarea->priv->statusbar, 0, text);
		g_object_ref (G_OBJECT (echoarea));
		g_timeout_add (timeout, pop_message, ctx);
	}
	else {
		if (echoarea->priv->perm_msg_active)
			gtk_statusbar_remove (echoarea->priv->statusbar, 0,
					      echoarea->priv->perm_id);
		echoarea->priv->perm_msg_active = TRUE;
		echoarea->priv->perm_id = gtk_statusbar_push (echoarea->priv->statusbar,
							      0, text);
	}

	rb_thread_helpers_unlock_gdk ();
}

static gboolean
pulse_status (gpointer data)
{
	RBEchoArea *echoarea = RB_ECHO_AREA (data);
	gdk_threads_enter ();

	if (echoarea->priv->active_tasks == 0) {
		gtk_progress_bar_set_fraction (echoarea->priv->progressbar, 1.0);

		g_object_unref (G_OBJECT (echoarea));

		gdk_threads_leave ();
		return FALSE;
	}

	gtk_progress_bar_pulse (echoarea->priv->progressbar);

	gdk_threads_leave ();
	return TRUE;
}

guint
rb_echo_area_begin_task (RBEchoArea *echoarea,
			 const char *text)
{
	struct RBEchoAreaTaskContext *ctx = g_new0 (struct RBEchoAreaTaskContext, 1);

	rb_thread_helpers_lock_gdk ();

	if (echoarea->priv->active_tasks == 0) {
		g_object_ref (G_OBJECT (echoarea));
		g_timeout_add (50, pulse_status, echoarea);
	}

	echoarea->priv->active_tasks++;
	echoarea->priv->active_tasks_ids++;

	ctx->id = echoarea->priv->active_tasks_ids;
	ctx->statusbar = GTK_STATUSBAR (gtk_statusbar_new ());

	gtk_statusbar_set_has_resize_grip (ctx->statusbar, FALSE);

	gtk_box_pack_start (GTK_BOX (echoarea), GTK_WIDGET (ctx->statusbar),
			    TRUE, TRUE, 0);
	gtk_box_reorder_child (GTK_BOX (echoarea),
			       GTK_WIDGET (ctx->statusbar), 1);
	gtk_statusbar_push (ctx->statusbar, 0, text);
	g_hash_table_insert (echoarea->priv->active_task_statusbars,
			     (gpointer) ctx->id,
			     ctx);

	rb_thread_helpers_unlock_gdk ();
	
	return ctx->id;
}

void
rb_echo_area_end_task (RBEchoArea *echoarea, guint id)
{
	struct RBEchoAreaTaskContext *ctx;

	rb_thread_helpers_lock_gdk ();

	ctx = g_hash_table_lookup (echoarea->priv->active_task_statusbars,
				   (gpointer) id);
	g_assert (ctx != NULL);
	gtk_container_remove (GTK_CONTAINER (echoarea),
			      GTK_WIDGET (ctx->statusbar));
	g_hash_table_remove (echoarea->priv->active_task_statusbars,
			     (gpointer) ctx->id);
	echoarea->priv->active_tasks--;

	rb_thread_helpers_unlock_gdk ();

	g_free (ctx);
}
