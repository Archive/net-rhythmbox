/*
 *  Copyright (C) 2003 Colin Walters <walters@debian.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id: rb-link.h,v 1.2 2002/08/02 23:25:27 jbaayen Exp $
 */

#ifndef __RB_ECHO_AREA_H
#define __RB_ECHO_AREA_H

#include <gtk/gtkstatusbar.h>

G_BEGIN_DECLS

#define RB_TYPE_ECHO_AREA         (rb_echo_area_get_type ())
#define RB_ECHO_AREA(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), RB_TYPE_ECHO_AREA, RBEchoArea))
#define RB_ECHO_AREA_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), RB_TYPE_ECHO_AREA, RBEchoAreaClass))
#define RB_IS_ECHO_AREA(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), RB_TYPE_ECHO_AREA))
#define RB_IS_ECHO_AREA_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), RB_TYPE_ECHO_AREA))
#define RB_ECHO_AREA_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), RB_TYPE_ECHO_AREA, RBEchoAreaClass))

typedef struct RBEchoAreaPrivate RBEchoAreaPrivate;

typedef struct
{
	GtkHBox parent;

	RBEchoAreaPrivate *priv;
} RBEchoArea;

typedef struct
{
	GtkHBoxClass parent;
} RBEchoAreaClass;

GType		rb_echo_area_get_type	(void);

RBEchoArea *	rb_echo_area_new	(void);

void		rb_echo_area_msg	(RBEchoArea *echoarea,
					 const char *msg);

void		rb_echo_area_msg_full	(RBEchoArea *echoarea,
					 const char *msg,
					 guint timeout);

guint		rb_echo_area_begin_task (RBEchoArea *echoarea,
					 const char *msg);

void		rb_echo_area_end_task	(RBEchoArea *echoarea,
					 guint id);

G_END_DECLS

#endif /* __RB_ECHO_AREA_H */
