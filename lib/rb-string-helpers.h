/*
 *  Copyright (C) 2002 Jorn Baayen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id: rb-string-helpers.h,v 1.6 2002/12/10 22:32:22 cwalters Exp $
 */

#ifndef __RB_STRING_HELPERS_H
#define __RB_STRING_HELPERS_H

G_BEGIN_DECLS

char *rb_prefix_to_suffix (const char *string);
int   rb_utf8_strncasecmp (gconstpointer a, gconstpointer b);
char *rb_unicodify	  (const char *str, gboolean try_iso1_first);


G_END_DECLS

#endif /* __RB_STRING_HELPERS_H */
